The goal of designing the Topic distiller is to alleviate the heavy load of analyzing vast
quantities of textual data. To reach said goal, a system able to extract
both apparent and latent semantic topics from textual documents ranging from
news articles to social media postings, is designed. Topic Distiller will enable
application developers to build applications such as text summarizers and semantic
search engines that further help professionals such as researchers, journalists,
and marketeers in their daily work.

\subsection{Requirements}\label{sec:requirements}
The aim of this thesis is to design and implement a tool that can distill a
list of topics from text documents, hence the name Topic Distiller.  The
implemented application also has to be able to detect topics that are latent in the
document so it has to have knowledge of a large database of different topics.
On top of that, the Topic Distiller has to provide an API for querying all available topics. These topics
could then be further used to index documents for semantic search. The indexing
is outside of the scope of this thesis.

An important use case for the our system is to identify the topics trending in
social media. Dealing with social media posts is hard since they are usually
short and do not follow the grammar rules strictly. The frequency at which new
data are produced by social media is quite high, posing another requirement
for the Topic Distiller: \emph{high throughput}. This means that the system
should be fast enough to be usable in real time social media analysis.

The sets of topics discovered by the Topic Distiller must be usable as they are
to minimize manual work; no human work should be required for checking that the
topics are correct. This means that the topics must represent real world
phenomena and can not be nonsensical.

Ideally, the final system will have as little code as possible to be maintained,
because more code will result in higher maintenance overhead and error
probability.

On top of all the above requirements, the system must provide an simple
interface for it to be easily integrated to other systems.

The requirements are summarized in the following list:

\begin{enumerate}
\item The Topic Distiller must be able to identify relevant and latent topics in a textual document.
\item The Topic Distiller must be able to extract topics from short texts such as social media postings.
\item The topics discovered by the Topic Distiller must represent real world phenomena.
\item The Topic Distiller must be able to process enough documents per minute.
\item The Topic Distiller should be easy to maintain.
\item The Topic Distiller must provide an simple interface for communicating
  with other systems.
\end{enumerate}

\subsection{System Design}\label{sec:system-design}
The block diagram of the Topic Distiller,
seen in Figure~\ref{fig:system-block-diagram}, contains the internal (squares
inside of the dotted line) and external (squares with round corners outside of
the dotted line) modules along with the communication between said modules.
Internal modules consist of software that is specially built for the Topic Distiller.
External modules are third-party services that exist independently of the Topic Distiller.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../images/topic_extractor_block_diagram.jpg}
\caption{\label{fig:system-block-diagram}
Block diagram of the system with its internal and external modules.}
\end{figure}

The main flow of the Topic Distiller is as follows: First, the system is
provided a text document. Second, EL is used to find all the DBpedia entity
mentions from said document. Third, a graph is built. Fourth, a centrality
measure is used along with lexical analysis to rank the nodes, and fifth, the top
ranked nodes are returned. It should be noted here that no lexical methods are
used to generate the list of topic candidates, only pure EL. Lexical methods are
only used to rank the already generated candidates.

Different parts of the application flow are delegated to different modules. The
API module is responsible for serving the functionality of the system to the
outside-world, satisfying the requirement number 6 (see Section~\ref{sec:requirements}). The ontology module employs EL to connect the document to
DBpedia. Employing EL guarantees that topics represent real world phenomena,
hence meeting the requirement number 3. The graph is built by the graph-module
connecting DBpedia entities found in the document to more abstract DBpedia
entities. This method of building the graph aims to satisfy the requirements 1
and 2.

In a famous paper, Donald E. Knuth said that ``premature optimization is the
root of all evil''~\cite{knuth1974structured}. To follow this advice no special
emphasis was dedicated to designing as computationally fast system as possible.
The design decision applied here is to first meet the other requirements and
then evaluate if the Topic Distiller needs optimization.

\subsubsection{Application Programming Interface (API)}\label{sec:api-design}
API provides a way of communication between the system and other systems. Users of the Topic Distiller send a request to the API to extract topics with the document that the topics are to be extracted from. The document is then sent to the Ontology module, described in Section~\ref{sec:ontology-design}, for processing.

The request is sent to the API using the Hyper Text Transfer Protocol's
(HTTP)~\cite{fielding1999hypertext} POST method. The payload of the POST method
is a JavaScript Object Notation (JSON)~\cite{bray2017javascript} object
containing two keys: \texttt{text} and \texttt{num\_topics}. The value of the
former is the text, from which the topics are to be extracted, and the latter is
the number of topics to be extracted.

The response sent from the API is also in JSON format and contains two keys,
\texttt{topics}, whose value is a JSON array of text strings representing the
extracted topics, and \texttt{success}, whose value is boolean \texttt{true} or
\texttt{false}, indicating the success or failure of topic extraction
respectively.

If the request is malformed, or an error occurs while processing the request, the \texttt{success} key will have \texttt{false} as its value and an error message is provided along with the corresponding HTTP error code.

Example JSONs of request, response and error can be found in~\cite{miika2019topic}.

\subsubsection{Ontology}\label{sec:ontology-design}
The Ontology module provides the necessary functions needed to handle all
actions regarding DBpedia. The first step is to find all the mentions of DBpedia
sources in the document received from the API module. To accomplish this task,
DBpedia Spotlight, described in Section~\ref{sec:dbpedia-spotlight}, is employed
by simply sending a JSON request to the service with the text that is to be
annotated along with the desired confidence and support values (see
Section~\ref{sec:dbpedia-spotlight}). The confidence and support used in
this work were 0.5 and 20 respectively. DBpedia Spotlight will then answer the request by sending a list of the annotated DBpedia resources. After receiving the requested annotations they are aggregated in a list that contains each unique annotation along with the number of times it appears in the document.

Following Hulpus et al.'s work~\cite{hulpus2013unsupervised} the \emph{n} most
frequent DBpedia resources are selected as \emph{seed concepts}. Then these seed
concepts queried against DBpedia to get all DBpedia resources connected to seed
concepts with the following edge types: \texttt{skos:broader},
\texttt{skos:broaderOf}, \texttt{rdf:subClassOf}, \texttt{dct:subject}. The same
procedure (a hop) is then applied to the found DBpedia resources recursively.
Hulpus et al. discovered that two hops is ideal and thusly in this work, only
two hops are used.
The found nodes and their relationships to other nodes form a list of edges, where nodes are the DBpedia resources and edges are the connections between those resources. This list of edges is passed to the Graph builder module for further processing.

\subsubsection{Graph Builder}\label{sec:graph-builder-design}
The Graph Builder module has only two tasks: creating the
graph from edges provided by the Ontology module, and ranking the nodes in the
graph using the degree centrality measure. The nodes represent the topic
candidates and the edges the connections between the topics.

The decision to use degree centrality
instead of focused information centrality or focused random walk centrality
proposed by Hulpus and Greene in~\cite{hulpus2013unsupervised} stems from
research conducted by Boudin in~\cite{boudin2013comparison}. Although, the
Bouding does not compare degree centrality to the measures proposed by Hulpus
and Greene, the Bouding concludes that ``degree centrality, despite being conceptually the simplest measure, achieves results comparable to the widely
used TextRank algorithm''.

After the graph is created and the nodes ranked, the nodes are passed to the
Ranker module for final ranking.

\subsubsection{Ranker}\label{sec:ranker-design}
The Ranker module devises the final ranking of the topic candidates. It does it
by calculating a \emph{topic overlap score} ($\mathrm{tos}$) for each topic and combining them with the degree centrality scores of each topic given by the Graph builder module. Topic overlap score is calculated with tokenizing the topic and calculating how many times each token in the topic appears in the document and dividing the calculated number by the length of the topic effectively normalizing the length of the topic. The formula of the topic overlap score is as seen in Equation~\ref{eq:topic-overlap-score}:
\begin{equation}
  \label{eq:topic-overlap-score}
  \mathrm{tos} = \dfrac{\mathrm{tf}}{|\mathrm{t}|} \text{,}
\end{equation}
where $\mathrm{tf}$ is the token frequency, or the number of times the token appears in
the document, and $|\mathrm{t}|$ is the length of the topic in tokens.

The final topic score ($\mathrm{fts}$) is then combined with the degree centrality score using the following formula:
\begin{equation}
\label{eq:final-topic-score}
\mathrm{fts} = \alpha \mathrm{dcs} + \beta \mathrm{tos} \text{,}
\end{equation}
where \(\alpha\) and \(\beta\)  are coefficients for adjusting the weight of the
degree centrality score ($\mathrm{dcs}$) and $\mathrm{tos}$ respectively. Following the
intuition that topics directly mentioned in the document are more
important than latent ones, the coefficient \(\beta\) is given a value of 1.5 and \(\alpha\) 1.0.

After the final score is calculated the topic candidates are ranked and $n$ highest scoring candidates are selected as the topics of the document.