\subsection{Entity Linking}
Entity Linking (EL) is defined by \cite{shen2015entity} as follows: ``[EL] is the task to link
entity mentions in text with their corresponding entities in a knowledge base''.
Entity linking is closely intertwined with \emph{named entity recognition}
(NER), which is the method of identifying \emph{named entities} or the names of
people, organizations, and geographic locations from text~\cite{grishman1996message}. These named entities can, for example, be entities
in a knowledge base, such as DBpedia. Here, we are going to look at the EL
problem in the context of linking named entities in a document to their
corresponding DBpedia entities.

NER is the first of two steps in EL, the second
being \emph{disambiguation}, which is the task of selecting the correct candidate of
multiple entities sharing the same name. Disambiguation has proven to be a
formidable problem in NER.\@ For example, consider the following text:

\begin{center}
\textit{After finishing her meal, she decided to visit Washington.}
\end{center}

Even for a human reader, with general education, it is not self evident if the
\emph{Washington} in the text refers to the capital city of the United States, the state in the United States, or the former
president of the United States. The Wikipedia disambiguation page for Washington lists over 30 cities with
that name, not
to mention all the persons with the same name.

The disambiguation process can be thought as a ranking problem: rank the
candidates and select the topic ranking one as the linked entity. One simple
approach is to rank the candidates by their popularity. For example, in case of linking
entities to DBpedia
the Wikipedia article view counts corresponding to the DBpedia entity could be used to rank the candidates. This method alone
is able to reach 71\% accuracy~\cite{ji2011knowledge}.

More sophisticated methods include context of the candidate entity into the
ranking process~\cite{bunescu2006using}. Context of a named entity is the document it appears in and
context of a DBpedia entity is its corresponding wikipedia article. Similarity
between the contexts can then be used rank the entities.


\subsection{Automatic Keyphrase Extraction}
AKE methods can be divided into two main categories: supervised and
unsupervised. Supervised methods require training data which is hard to come by
and very expensive to produce. In this work we will be focusing on the
unsupervised methods. For more detailed review of the state-of-the-art in AKE
look for \cite{miika2019topic}. Most of the research into unsupervised AKE methods has
been focusing on graph-based methods. The idea is to form a graph where the
nodes are represented by the keyphrase candidates and the edges represent the
connections between those candidates.

The earliest graph-based AKE method is TextRank (TR)~\cite{mihalcea2004textrank},
which employs the notion of PageRank algorithm~\cite{page1999pagerank} that an
important node in the graph is connected to a large number of other nodes and to
nodes that are important themselves. PositionRank (PR)
\cite{florescu2017positionrank} algorithm augments TR by taking into
account the position of each keyphrase in the document. Another version of TR, called ExpandRank~\cite{wan2008single}, encodes neighbouring documents to employ more information on the keyphrase extraction task. This method of using neighboring documents is only viable if a corpus of related documents is available.

According to~\cite{liu2010automatic}, graph-based methods suffer from two disadvantages:
\begin{enumerate}
\item Graph-based models rank high keyphrases that are strongly connected. This does not ensure that they are relevant to the main topics of the document.
\item Ideally, a good set of keyphrases should contain all the main topics of the document. Graph-based methods do not guarantee this.
\end{enumerate}

KeyCluster~\cite{liu2009clustering} attempts to overcome these disadvantages by grouping semantically similar candidates using Wikipedia and co-occurence based statistics. Even though KeyCluster is able to outperform TR it suffers from the problem that all topics are treated equally. Ideally, only important topics should be represented as keyphrases.

Topical PageRank (TPR)~\cite{liu2010automatic} is a successful attempt
to alleviate the problems of KeyCluster. The approach employs the latent Dirichlet allocation (LDA)~\cite{blei2003latent} topic model as
a way to incorporate out-of-corpus knowledge to enhance keyphrase extraction. TPR also outperforms TR but unfortunately the authors did not compare it to KeyCluster.

The original version of TPR is very expensive computationally since PageRank itself is a costly algorithm to run and it has to be run for each topic model. To mitigate the computational cost~\cite{sterckx2015topical} propose a modification to TPR that runs PageRank only once per document achieving nearly equal performance compared to the original TPR while significantly cutting down the computational costs. 

\subsection{Automatic Topic Labeling}
Topic modeling is a way to identify the subject matter of a collection of
documents by determining its inherently addressed
topics~\cite{hulpus2013unsupervised}. Simply put, a topic model is a collection
of terms explicitly appearing in a set of documents and their corresponding
marginal probabilities to appear in a given topic. Marginal probability is
defined as the probability of a term to appear in a topic. Most used techniques for topic modeling include LDA~\cite{blei2003latent}, \emph{latent semantic analysis} (LSA)~\cite{dumais1988using}, and \emph{probabilistic latent semantic analysis} (PLSA)~\cite{hofmann1999probabilistic}.

The problem with topic modeling is that the resulting topics are hardly interpretable by humans. One standard way of making sense of a topic is to extract the top ten terms with the highest probability. This results in term lists that are labor intensive to decipher. Automatic topic labeling aims to tackle this problem by finding labels for a given topic.

ATL methods can be divided into non-graph-based and graph-based methods. Here, we'll focus on the graph-based methods but a more detailed survey on different ATL methods can be found in~\cite{miika2019topic}.

OntoLDA~\cite{allahyari2009ontolda} is an ATL method that integrates ontological concepts with topic models. This is done by building a semantic graph by extracting the top ranking concepts of a document as vertices of the graph, based on their marginal probability, then connecting these vertices by edges if they are related in the DBpedia ontology. The semantic graph consists of subcomponents called thematic graphs which are the connected components of the semantic graph. The nodes of the thematic graph with most connections are ranked using the Hyperlink-Induced Topic Search (HITS)~\cite{kleinberg1999hubs} algorithm and these nodes are named as the core concepts of the topic. Further, a topic graph is extracted by traversing DBpedia at most three hops away from each core concepts and the union of these topic graphs is called the topic label graph. The core concepts of a label are treated as the topic label candidates and each of these candidates is scored using three metrics: membership score, coverage score and semantic similarity, all explained in detail in~\cite{allahyari2009ontolda}.

Similar, albeit simpler, method compared to OntoLDA is a method proposed by Hulpus et al. in \cite{hulpus2013unsupervised}. This method also incorporates DBpedia, but instead of proposing their own algorithm for topic extraction, Hulpus et al. use the conventional LDA algorithm. The topic terms produced by LDA are disambiguated by using a word sense disambiguation (WSD) method proposed by Hulpus et al. in their previous work~\cite{hulpus1eigenvalue}. Each of these disambiguated topic terms is then used along with DBpedia to build a sense graph and the union of these sense graphs is used with a network centrality measure to rank the disambiguated terms. Highest ranked terms are the topic label candidates.