This section describes the technologies used in the design and
implementation of Topic Distiller.

\section{Ontologies}\label{sec:ontologies}
In philosophy, ontology refers to the theory of the nature of existence; what exists and how it exists. Artificial intelligence and web researchers have claimed the term using it to refer to a document or a file that formally defines relations between terms~\cite{berners2001semantic}. These ontologies serve as machine-readable sources of information, making them highly usable for automatic information processing.

Ontologies for information processing originally arose from the growing need of data integration in
disciplines such as genomics, proteomics, and
epidemology~\cite{shadbolt2006semantic} and they were domain specific. The
vision of Berners-Lee et al.~\cite{berners2001semantic} is to combine these
ontologies into a one all-encompassing semantic web. There are several attempts
at creating general, domain independent ontologies which include
DBpedia~\cite{auer2007dbpedia},
Freebase~\cite{bollacker2008freebase}, OpenCyg~\cite{opencyg}, and Yago~\cite{suchanek2007yago}. These general ontologies are convenient for applications such as knowledge discovery and document annotation.

\subsection{DBpedia}\label{sec:dbpedia}
DBpedia is a freely available database of structured data extracted from Wikipedia. The creators of DBpedia, namely Auer et al.~\cite{auer2007dbpedia}, define it as follows: ``DBpedia is a community effort to extract structured information from Wikipedia and to make this information available on the Web''.

DBpedia takes data from Wikipedia and converts it to \emph{Resource Description Framework} (RDF)~\cite{miller1998introduction}. In RDF data are represented as \emph{triples} that consist of \emph{subject}, \emph{predicate}, and \emph{object}, or \emph{resource}, \emph{property type}, and \emph{property value}. The subject is always a resource, e.g.~DBpedia entity. The object can be a resource or just an atomic value like a birth date. The predicate or property type is the link between the predicate and object. For example, \emph{Jay Z} is the \emph{spouse} of \emph{Beyoncé}.

A database, such as DBpedia, that is structured in RDF format can be thought as a large network graph where all resources have properties and are connected to other resources by these properties. This allows for sophisticated analyses of relations between resources using tools such as graph theory, and social network theory.

\section{Entity Linking}\label{sec:entity-linking}
EL is defined by~\cite{shen2015entity} as follows: ``[EL] is the task to link
entity mentions in text with their corresponding entities in a knowledge base''.
Entity linking is closely intertwined with \emph{named entity recognition}
(NER), which is the method of identifying \emph{named entities} or the names of
people, organizations, and geographic locations from text~\cite{grishman1996message}. These named entities can, for example, be entities
in a knowledge base, such as DBpedia. Here, we are going to look at the EL
problem in the context of linking named entities in a document to their
corresponding DBpedia entities.

NER is the first of two steps in EL, the second
being \emph{disambiguation}, which is the task of selecting the correct candidate of
multiple entities sharing the same name. Disambiguation has proven to be a
formidable problem in NER.

The disambiguation process can be thought as a ranking problem: rank the
candidates and select the topic ranking one as the linked entity. One simple
approach is to rank the candidates by their popularity. For example, in case of linking
entities to DBpedia
the Wikipedia article view counts corresponding to the DBpedia entity could be used to rank the candidates. This method alone
is able to reach 71\% accuracy~\cite{ji2011knowledge}.

\subsection{DBpedia Spotlight}\label{sec:dbpedia-spotlight}
DBpedia spotlight (DBPS) is a system that links DBpedia entities to documents~\cite{mendes2011dbpedia}. Originally DBPS was only capable of annotating English
documents but the new, improved version~\cite{daiber2013improving} works with
other languages, such as German, Dutch, French, Russian, and Turkish, among others. DBPS divides the task of
entity-linking into three stages. The first is the \emph{spottig stage} that
recognizes phrases that may include a mention to a DBpedia entity.
\emph{Candidate selection} stage links those mentions to the DBpedia and
\emph{disambiguation} phase selects the most suitable entity among the candidates using the
context around the spotted phrases.

A demo with a graphical user
interface\footnote{https://www.dbpedia-spotlight.org/demo/} is provided to test
the service along with a public API\footnote{https://www.dbpedia-spotlight.org/api} that can be used to programmatically
annotate textual documents.

\section{Automatic Keyphrase Extraction}\label{sec:automatic-keyphrase-extraction}
Automatic keyphrase extraction (AKE) is defined by~\cite{turney2000learning}
``as the automatic selection of important, topical phrases from within the body
of a document''. In other words, the purpose of automatic keyphrase extraction
is to find a list of phrases that capture the topics that are related to the
document in question. The extracted keyphrases are used further to improve other natural language processing (NLP) and information retrieval (IR) tasks such as text summarization~\cite{das2007survey}, text categorization~\cite{lam1999automatic}, opinion mining~\cite{berend2011opinion}, and document indexing~\cite{gutwin1999improving}.

AKE can be split into supervised and unsupervised AKE. The most common
supervised techniques, found in the literature, include \emph{task reformulation}
\cite{frank1999domain}, \cite{witten2005kea}, \cite{turney2002learning}, \cite{jiang2009ranking},
\emph{feature design} \cite{witten2005kea}, \cite{frank1999domain},
\cite{nguyen2007keyphrase}, \cite{yih2006finding}, \cite{barker2000using},
\cite{hulth2003improved}, \cite{medelyan2009human}, \cite{turney2003coherent},
and \emph{deep learning} \cite{zhang2016keyphrase},
\cite{basaldella2018bidirectional}, \cite{meng2017deep}. Unsupervised methods
include \emph{graph-based ranking} \cite{mihalcea2004textrank},
\cite{florescu2017positionrank}, \cite{wan2008single},
\cite{bougouin2013topicrank}, \cite{boudin2018unsupervised},
\cite{liu2009clustering}, \cite{liu2010automatic}, \cite{sterckx2015topical},
\cite{grineva2009extracting}, \emph{summarization} \cite{zha2002generic},
\cite{wan2007towards}, \emph{language modeling}, \cite{tomokiyo2003language},
\cite{mahata2018key2vec}, \cite{bennani2018embedrank}.

Supervised methods require human annotated training data which is hard to come
by and very expensive to produce. This facts gives advantage to unsupervised
methods that require no training data what so ever. The most successful
unsupervised methods in the literature are graph-based.

\section{Automatic Topic Labeling}\label{sec:automatic-topic-labeling}

Topic modeling is a way to identify the subject matter of a collection of
documents by determining its inherently addressed
topics~\cite{hulpus2013unsupervised}. Simply put, a topic model is a collection
of terms explicitly appearing in a set of documents and their corresponding
marginal probabilities to appear in a given topic. Marginal probability is
defined as the probability of a term to appear in a topic.

Most used techniques for topic modeling include LDA~\cite{blei2003latent}, \emph{latent semantic analysis} (LSA)~\cite{dumais1988using}, and \emph{probabilistic latent semantic analysis} (PLSA)~\cite{hofmann1999probabilistic}. 

The problem with topic modeling is that the resulting topics are hardly interpretable by humans. One standard way of making sense of a topic is to extract the top ten terms with the highest probability. This results in term lists like the following:
\begin{center}
\textit{stock, market, investor, fund, trading, investment, firm, exchange, companies, share.}
\end{center}

With a little inspection, it is easy to see that the above list of terms belongs under a topic of \emph{stock market trading}~\cite{lau2011automatic}. \emph{Automatic topic labeling} (ATL) is a research area which focuses on finding terms such as \emph{stock market trading} for a corresponding topic. The idea is to alleviate the cognitive load for humans to interpret raw topic term listings by explicitly identifying the semantics of the topic~\cite{lau2011automatic}.

ATE can be divided into two sub categories: \emph{graph-based}
\cite{allahyari2009ontolda}, \cite{hulpus2013unsupervised},
\cite{aletras2014labelling}, and \emph{non-graph-based} \cite{mei2007automatic},
\cite{lau2011automatic}, \cite{bhatia2016automatic}.


% Both AKE and ATL have similar objectives and methods to accomplish the said objectives. AKE is conventionally used to find keyphrases from a single document where topic modeling with ATL focuses on distilling information of document collections into phrases that capture their core ideas. Both of these objectives, in their core, aim to decrease the cognitive load of human beings trying to make sense of vast quantities of textual data. To tackle the difficult problems faced by both of these research fields, researchers have used methods, such as graph theory and word embeddings, among others.

% The trend in both AKE and ATL is to move from simple extractive methods to more
% abstractive methods. Just like human annotators, methods using
% external knowledge
% are able to find the broader topics related to the documents they process.

% Further strengthening the case for using knowledge bases, Hasan et al.~\cite{hasan2014automatic} suggest that incorporating background
% knowledge to AKE systems might be able to alleviate overgeneration, infrequency,
% and redundancy errors dicussed in Section~\ref{sec:shortcomings-ake}.

% AKE methods are commonly divided into supervised and unsupervised methods.
% Supervised methods perform well if they are fed a large quantity of good quality
% data. This means that if no such data is available the results are suboptimal,
% making supervised methods impractical for many use cases. No redemption is gained by the fact that most freely available datasets for AKE contain only
% scientific articles. Unsupervised methods do not need any
% training data by definition but they require more human ingenuity to bring in good
% results. A lot of effort by researchers has been aimed to making these
% unsupervised methods better and all of the state-of-the-art methods, excluding
% methods employing deep learning, are unsupervised. Deep learning would be a
% valid method to consider if building a large dataset was in the scope of this thesis.

% Most of the unsupervised
% methods for AKE in the literature are graph-based. This attribute makes them a natural match for graph-based knowledge bases, such as
% DBpedia. AKE methods use different lexical features to build graphs, as discussed in
% Section~\ref{sec:ake-unsupervised}. One problem with this approach is that
% sometimes the keyphrases extracted from the document are nonsensical; they do
% not represent any real word phenomena.

% In this thesis, it is argued that instead
% of using lexical features to build these graphs it is better to employ EL to
% find all the mentions of knowledge base entities from a text document and build the graph
% using the predicates connecting those entities. In other words, the knowledge
% base entities would be the nodes in the graphs and the predicates connecting
% those entities would be the edges. Building the graph in this manner is inspired by the
% ATL methods developed by Allahyari et al.~\cite{allahyari2009ontolda} and Hulpus
% et al.~\cite{hulpus2013unsupervised} discussed in
% Section~\ref{sec:graph-based-atl}. However, Allahyari et al. and Hulpus et al.
% do not employ EL to get the nodes. Instead, they rely on topical analysis.

% Using EL with knowledge bases ensures that all the keyphrases, or
% topics, found in the document would be existing, real world topics, e.g.~when using DBpedia as the knowledge base the topics found would be Wikipedia
% article titles. EL has the disadvantage over lexical methods in that it lacks the ability to extract new or obscure keyphrases that can not be found from the
% knowledge base. On the other hand, connecting documents to a knowledge base using
% EL enables finding latent topics by traversing the entities in the knowledge base to
% generate a larger graph that comprises of entities not present in the document,
% but closely related to it.