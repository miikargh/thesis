(TeX-add-style-hook
 "dt"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("dithesis" "a4paper" "12pt" "titlepage")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english" "finnish") ("inputenc" "utf8") ("fontenc" "T1") ("url" "hyphens") ("ulem" "normalem") ("hyphsubst" "english=nohyphenation" "finnish=nohyphenation") ("hyphenat" "none")))
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (TeX-run-style-hooks
    "latex2e"
    "abbreviations"
    "symbols"
    "introduction"
    "related_work"
    "design"
    "evaluation"
    "discussion"
    "conclusion"
    "appendix"
    "dithesis"
    "dithesis12"
    "babel"
    "inputenc"
    "fontenc"
    "times"
    "tabularx"
    "graphicx"
    "floatrow"
    "enumerate"
    "placeins"
    "fancybox"
    "verbatim"
    "longtable"
    "di"
    "url"
    "boxedminipage"
    "subcaption"
    "multirow"
    "amsfonts"
    "xcolor"
    "epstopdf"
    "capt-of"
    "amsmath"
    "wrapfig"
    "rotating"
    "ulem"
    "textcomp"
    "amssymb"
    "fancyvrb"
    "enumitem"
    "hyphsubst"
    "hyphenat")
   (LaTeX-add-labels
    "ch:related_work"
    "ch:design"
    "ch:evaluation"
    "ch:discussion"
    "ch:conclusion"
    "ch:appendix")
   (LaTeX-add-bibliographies
    "di")
   (LaTeX-add-array-newcolumntypes
    "L"))
 :latex)

