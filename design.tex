The goal of this thesis is to alleviate the heavy load of analyzing vast
quantities of textual data. To reach said goal, a system able to extract
both apparent and latent semantic topics from textual documents ranging from
news articles to social media postings, is designed. This system, named the
Topic Distiller, will enable
application developers to build applications such as text summarizers and semantic
search engines that further help professionals such as researchers, journalists,
and marketeers in their daily work.

The system architecture described in this section is for an application that
provides a programmable interface to enable communication with other
services, such as graphical user interfaces or document indexers. This chapter first
identifies the requirements for the system (see
Section~\ref{sec:requirements}), followed by design principles (see
Section~\ref{sec:design-principles}), and then moves on to describe the
composition of said system (see Section~\ref{sec:system-design}).

\section{Requirements}\label{sec:requirements}
The aim of this thesis is to design and implement a tool that can distill a
list of topics from text documents, hence the name Topic Distiller.  The
implemented application also has to be able to detect topics that are latent in the
document so it has to have knowledge of a large database of different topics.
On top of that, the Topic Distiller has to provide an API for querying all available topics. These topics
could then be further used to index documents for semantic search. The indexing
is outside of the scope of this thesis.

An important use case for the our system is to identify the topics trending in
social media. Dealing with social media posts is hard since they are usually
short and do not follow the grammar rules strictly. The frequency at which new
data are produced by social media is quite high, posing another requirement
for the Topic Distiller: \emph{high throughput}. This means that the system
should be fast enough to be usable in real time social media analysis.

The sets of topics discovered by the Topic Distiller must be usable as they are
to minimize manual work; no human work should be required for checking that the
topics are correct. This means that the topics must represent real world
phenomena and can not be nonsensical.

Ideally, the final system will have as little code as possible to be maintained,
because more code will result in higher maintenance overhead and error
probability.

On top of all the above requirements, the system must provide an simple
interface for it to be easily integrated to other systems.

The requirements are summarized in the following list:

\begin{enumerate}
\item The Topic Distiller must be able to identify relevant and latent topics in a textual document.
\item The Topic Distiller must be able to extract topics from short texts such as social media postings.
\item The topics discovered by the Topic Distiller must represent real world phenomena.
\item The Topic Distiller must be able to process enough documents per minute.
\item The Topic Distiller should be easy to maintain.
\item The Topic Distiller must provide an simple interface for communicating
  with other systems.
\end{enumerate}

\section{Design Principles}\label{sec:design-principles}
This section describes the design principles adopted to ensure that the
requirement of easy maintenance (see Section~\ref{sec:requirements}) is met.

To fulfill said requirement, a modular design is adopted. Each module is responsible for doing just one job and doing it well. The modules
consist of a set of functions that implement the necessary functionality of the
modules. To ensure encapsulation, interfaces
between these modules, and functions, should be built in such a manner that swapping the
internals of one module would not require changes in other modules. This
modularity will provide for easy testing and thus reduce the number of software
bugs. The system will also be stateless further diminishing the complexity of
the system.

Reinventing the wheel is a fool's errand; free and open-source software will be
used extensively. This will reduce the amount of maintainable code in
the system by a great degree.

The design principles applied in the implementation of the Topic Distiller are
summarized in the following list:

\begin{enumerate}
\item Modularity
\item Statelessness
\item Use of open-source software
\end{enumerate}

Python was chosen as the programming language since it provides good support for
the design principles listed above. Python makes it easy to organize software
into self-containing
modules\footnote{https://docs.python.org/3/tutorial/modules.html}, Python is
fully open-source, and has a large community providing quality libraries for
scientific computation.

% The system proposed in this thesis differs from the one proposed by Hulpus et
% al.~in two major ways. First, degree centrality measure is used to rank
% candidate topics instead of focused information centrality and focused random
% walk betweennes centrality. The motivation behind this design decision is that
% degree centrality is found by Bouding and Florian~\cite{boudin2013comparison} to
% be the best centrality metric for keyphrase extraction and that degree
% centrality is very simple to implement and found in the most popular network
% graph libraries. Second, instead of using LDA on a corpus of documents with
% DBpedia linking and disambiguation to attain the core concepts, the system
% described here uses DBpedia Spotlight~\cite{mendes2011dbpedia} to link DBpedia
% entities in the  the graph on top of said entities.

% Other differences of the Topic Distiller and the work of Hulpus et al.~\cite{hulpus2013unsupervised} include different scoring algorithm to rank the candidates, and using different DBpedia property types to build the graph. 

\section{System Design}\label{sec:system-design}
This section describes the design of the Topic Distiller, which consists of four
internal modules and two external modules. The block diagram of the designed system,
seen in Figure~\ref{fig:system-block-diagram}, contains the internal (squares
inside of the dotted line) and external (squares with round corners outside of
the dotted line) modules along with the communication between said modules.
Internal modules consist of software that is specially built for the Topic Distiller.
External modules are third-party services that exist independently of the Topic Distiller.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/topic_extractor_block_diagram.jpg}
\caption{\label{fig:system-block-diagram}
Block diagram of the system with its internal and external modules.}
\end{figure}

The main flow of the Topic Distiller is as follows: First, the system is
provided a text document. Second, EL is used to find all the DBpedia entity
mentions from said document. Third, a graph is built. Fourth, a centrality
measure is used along with lexical analysis to rank the nodes, and fifth, the top
ranked nodes are returned. It should be noted here that no lexical methods are
used to generate the list of topic candidates, only pure EL.\@ Lexical methods are
only used to rank the already generated candidates.

Different parts of the application flow are delegated to different modules. The
API module is responsible for serving the functionality of the system to the
outside-world, satisfying the requirement number 6 (see Section~\ref{sec:requirements}). The ontology module employs EL to connect the document to
DBpedia. Employing EL guarantees that topics represent real world phenomena,
hence meeting the requirement number 3. The graph is built by the graph-module
connecting DBpedia entities found in the document to more abstract DBpedia
entities. This method of building the graph aims to satisfy the requirements 1
and 2.

In a famous paper, Donald E. Knuth said that ``premature optimization is the
root of all evil''~\cite{knuth1974structured}. To follow this advice no special
emphasis was dedicated to designing as computationally fast system as possible.
The design decision applied here is to first meet the other requirements and
then evaluate if the Topic Distiller needs optimization.

The following
sections~\ref{sec:api-design},~\ref{sec:ontology-design},~\ref{sec:graph-builder-design},
and~\ref{sec:ranker-design}
will describe the design of the modules in more detail.

\subsection{Application Programming Interface (API)}\label{sec:api-design}
API provides a way of communication between the system and other systems. Users of the Topic Distiller send a request to the API to extract topics with the document that the topics are to be extracted from. The document is then sent to the Ontology module, described in Section~\ref{sec:ontology-design}, for processing.

The request is sent to the API using the Hyper Text Transfer Protocol's
(HTTP)~\cite{fielding1999hypertext} POST method. The payload of the POST method
is a JavaScript Object Notation (JSON)~\cite{bray2017javascript} object
containing two keys: \texttt{text} and \texttt{num\_topics}. The value of the
former is the text, from which the topics are to be extracted, and the latter is
the number of topics to be extracted. An example request JSON object is as seen
in Figure~\ref{src:example-json-request}. Note that the lines 2 and 3 of the
figure are actually one line split into to using the separator
\texttt{\textbackslash\textbackslash}. This is only for presentation purposes.
Valid JSON does not permit such formatting.

\begin{figure}
\begin{Verbatim}[numbers=left,xleftmargin=5mm]
{
    "text": "Software is a gas; \\
             it expands to fill its container.",
    "num_topics": 3
}
\end{Verbatim}
\caption{\label{src:example-json-request}
  Example request JSON.}
\end{figure}

The response sent from the API is also in JSON format and contains two keys, \texttt{topics}, whose value is a JSON array of text strings representing the extracted topics, and \texttt{success}, whose value is boolean \texttt{true} or \texttt{false}, indicating the success or failure of topic extraction respectively. An example response JSON can be seen in Figure~\ref{src:example-json-response}.


\begin{figure}
\begin{Verbatim}[numbers=left,xleftmargin=5mm]
{
    "topics": [
        "Software",
        "Computer science",
        "Nathan Myhrvold"
    ],
    "success": true
}
\end{Verbatim}
\caption{\label{src:example-json-response}
Example response JSON.}
\end{figure}

If the request is malformed, or an error occurs while processing the request, the \texttt{success} key will have \texttt{false} as its value and an error message is provided along with the corresponding HTTP error code. An example JSON of failed request can be seen in Figure~\ref{src:example-json-failure}.

\begin{figure}
\begin{Verbatim}[numbers=left,xleftmargin=5mm]
{
    "success": false,
    "error_message": "Malformed request.",
    "error_code": 400
}
\end{Verbatim}
\caption{\label{src:example-json-failure}
  Example failure response JSON.}
\end{figure}


\subsection{Ontology}\label{sec:ontology-design}
The Ontology module provides the necessary functions needed to handle all
actions regarding DBpedia. The first step is to find all the mentions of DBpedia
sources in the document received from the API module. To accomplish this task,
DBpedia Spotlight, described in Section~\ref{sec:dbpedia-spotlight}, is employed
by simply sending a JSON request to the service with the text that is to be
annotated along with the desired confidence and support values (see
Section~\ref{sec:dbpedia-spotlight}). An example query can be seen in
Figure~\ref{src:example-spotlight-query}. The confidence and support used in
this thesis were 0.5 and 20 respectively. DBpedia Spotlight will then answer the request by sending a list of the annotated DBpedia resources. After receiving the requested annotations they are aggregated in a list that contains each unique annotation along with the number of times it appears in the document.

\begin{figure}
\begin{Verbatim}[numbers=left,xleftmargin=5mm]
    {
        "text": "You're a wizard, Harry!",
        "confidence": 0.5,
        "support": 20
    }
\end{Verbatim}
\caption{\label{src:example-spotlight-query}
  Example of a request JSON to DBpedia Spotlight with parameters.}
\end{figure}

Following Hulpus et al.'s work~\cite{hulpus2013unsupervised} the \emph{n} most frequent DBpedia resources are selected as \emph{seed concepts}. DBpedia is then queried using the SPARQL query in Figure~\ref{src:sparql-related-resources} to find related DBpedia resources:

\begin{figure}
\begin{Verbatim}[numbers=left,xleftmargin=5mm]
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dct: <http://purl.org/dc/terms/>
SELECT *
WHERE {
    OPTIONAL { <%s> skos:broader ?broader } .
    OPTIONAL { <%s> skos:broaderOf ?broaderOf } .
    OPTIONAL { <%s> rdf:subClassOf ?subClassOf } .
    OPTIONAL { <%s> dct:subject ?subject } .
}
\end{Verbatim}
\caption{\label{src:sparql-related-resources}
SPARQL query for finding related resources.}
\end{figure}

In the SPARQL query of Figure~\ref{src:sparql-related-resources} all properties
of type \texttt{skos:broader}, \texttt{skos:broaderOf}, \texttt{rdf:subClassOf}, and \texttt{dct:subject} are selected from a given resource. The \texttt{\%s} symbol in the query is Python syntax for text formatting. The \texttt{\%s} is replaced by a resource that is going to be queried against. 

This query finds the related DBpedia resources that are connected to the seed
concepts with one of the following edge types: \texttt{skos:broader},
\texttt{skos:broaderOf}, \texttt{rdf:subClassOf}, \texttt{dct:subject}. The same
query is then used to find the related resources of the related resources of the
seed concepts. One of these queries is called a hop. It was empirically
discovered by Hulpus et al.~\cite{hulpus2013unsupervised} that two hops is ideal and thusly in this work, only two hops are used. The found nodes and their relationships to other nodes form a list of edges, where nodes are the DBpedia resources and edges are the connections between those resources. This list of edges is passed to the Graph builder module for further processing.

\subsection{Graph Builder}\label{sec:graph-builder-design}
The simplest of the modules is the Graph Builder. It has only two tasks: creating the
graph from edges provided by the Ontology module, and ranking the nodes in the
graph using the degree centrality measure. The nodes represent the topic
candidates and the edges the connections between the topics.

The decision to use degree centrality
instead of focused information centrality or focused random walk centrality
proposed by Hulpus and Greene in~\cite{hulpus2013unsupervised} stems from
research conducted by Boudin in~\cite{boudin2013comparison}. Although, the
Bouding does not compare degree centrality to the measures proposed by Hulpus
and Greene, the Bouding concludes that ``degree centrality, despite being conceptually the simplest measure, achieves results comparable to the widely
used TextRank algorithm''.

After the graph is created and the nodes ranked, the nodes are passed to the
Ranker module for final ranking.

\subsection{Ranker}\label{sec:ranker-design}
The Ranker module devises the final ranking of the topic candidates. It does it
by calculating a \emph{topic overlap score} ($\mathrm{tos}$) for each topic and combining them with the degree centrality scores of each topic given by the Graph builder module. Topic overlap score is calculated with tokenizing the topic and calculating how many times each token in the topic appears in the document and dividing the calculated number by the length of the topic effectively normalizing the length of the topic. The formula of the topic overlap score is as seen in Equation~\ref{eq:topic-overlap-score}:
\begin{equation}
  \label{eq:topic-overlap-score}
  \mathrm{tos} = \dfrac{\mathrm{tf}}{|\mathrm{t}|} \text{,}
\end{equation}
where $\mathrm{tf}$ is the token frequency, or the number of times the token appears in
the document, and $|\mathrm{t}|$ is the length of the topic in tokens.

The final topic score ($\mathrm{fts}$) is then combined with the degree centrality score using the following formula:
\begin{equation}
\label{eq:final-topic-score}
\mathrm{fts} = \alpha \mathrm{dcs} + \beta \mathrm{tos} \text{,}
\end{equation}
where \(\alpha\) and \(\beta\)  are coefficients for adjusting the weight of the
degree centrality score ($\mathrm{dcs}$) and $\mathrm{tos}$ respectively. Following the
intuition that topics directly mentioned in the document are more
important than latent ones, the coefficient \(\beta\) is given a value of 1.5 and \(\alpha\) 1.0.

After the final score is calculated the topic candidates are ranked and $n$ highest scoring candidates are selected as the topics of the document.

\section{Implementation}\label{sec:implementation}
This section describes how the system was implemented to match the requirements
in Section~\ref{sec:requirements} and the system design presented in Section~\ref{sec:system-design}.

\subsection{Tools and Libraries}
The programming language chosen for this project was Python (version 3.6). As
noted in Section~\ref{sec:design-principles}, Python is a highly modular language with a large collection of standard libraries. Scientific computing community has been using Python for a long time building a large quantity of libraries suitable for NLP.~Python is also very expressive and rather easy to learn, enabling easy collaboration with other programmers in the future of a project.

The libraries used in this work can be found in Table~\ref{tab:project-libraries}.

\begin{table}[htbp]
\caption{\label{tab:project-libraries}
Project libraries}
\centering
\begin{tabularx}{\linewidth}{llL}
Library & Usage & Description\\
\hline
spaCy \cite{spacy} & NLP & Provides a large set of functions for NLP\\
aiosparql \cite{aiosparql} & SPARQL & Provides an easy way to integrate SPARQL queries in python\\
NetworkX \cite{networkx} & Graphs & Provides functions for building and analysing network graphs\\
Flask \cite{flask} & API & Provides a set of utilities for building Web APIs\\
 &  & \\
\end{tabularx}
\end{table}

\begin{itemize}
\item\textbf{spaCy} is an ``Industrial-Strength Natural Language Processing
  library''~\cite{spacy}. The authors of the library claim it is the fastest NLP
  library in the world, in terms of computing time. spaCy provides a simple and
  unified API for its functions making it very easy to use. These attributes
  make up for a strong case for using spaCy rather than the more conventional NLTK library. In this thesis, spaCy is used for text tokenization.

\item\textbf{aiosparql} is an asynchronous SPARQL wrapper for Python~\cite{aiosparql}. It provides an easy way to integrate SPARQL queries to Python code. There are other SPARQL wrappers for Python but aiosparql is the only one providing asynchronous network calls making network call optimization effortless.

\item\textbf{NetworkX} is a ``Python package for the creation, manipulation, and study of the structure, dynamics, and functions of complex networks''~\cite{networkx}. It is used in this project for building the graphs and analyzing the centrality of the nodes in the graphs.

\item\textbf{Flask} is a micro-framework for Python for building web APIs~\cite{flask}. This minimalist framework is very modular enabling the use of only those functions needed thus making APIs build with Flask light and simple.
\end{itemize}

\subsection{Internal Modules}
The Python code in this work is split into modules, each providing a set of
functions that belong to a block seen in Figure~\ref{fig:system-block-diagram}.
The design of the modules can be found in Section~\ref{sec:system-design} and a data
flow diagram for the same modules can be seen in
Figure~\ref{fig:topic-distiller-dfd}. The colored blocks of
Figure~\ref{fig:topic-distiller-dfd} are the internal modules of the Topic
Distiller. The white circles represent the main functions of the said modules and the
arrows represent the data flow between the functions. The sizes and colors of the blocks
and circles do not carry any specific meaning.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/topic_distiller_dfd.png}
\caption{\label{fig:topic-distiller-dfd}
Internal modules data flow diagram.}
\end{figure}

\subsubsection{API}
This module (the yellow box top-left in Figure~\ref{fig:topic-distiller-dfd}) simply exposes the \texttt{extract\_topics} function to outside
world with HTTP protocol using the \emph{Flask} micro-framework. The
\texttt{extract\_topics} function takes only two arguments: the text to be
analyzed and the number of topics to be returned. Detailed explanation of the
design of the API can be found in Section~\ref{sec:api-design}.

\subsubsection{Ontology}
This module (the red box top-right in Figure~\ref{fig:topic-distiller-dfd}) connects the rest of the system to DBpedia. It provides the SPARQL
commands needed to traverse DBpedia along with the logic to do so. The main functions in the Ontology module are \texttt{extract\_dbpedia\_ents}, which queries DBpedia Spotlight for annotating mentions of DBpedia entities in the text, and \texttt{get\_edges}, which creates a list of linked DBPedia entities by recursively querying DBpedia given the entities annotated by \texttt{extract\_dbpedia\_ents}. The implementation of the Ontology module relies on the \emph{aiosparql} library.

\subsubsection{Graph Builder}
The Graph Builder module (the green box bottom in Figure~\ref{fig:topic-distiller-dfd}) comprises of functions that are used to build and analyse the network graph from the list of connected edges provided by the \texttt{get\_edges} function of the Ontology module. The main functions of the Graph Module are \texttt{create\_graph}, which takes in a list of edges and builds a graph from them, and \texttt{sort\_nodes} which sorts the nodes by their degree centrality. The implementation of the Graph Builder module utilises the \emph{NetworkX} module.

\subsubsection{Ranker}
The Ranker module (the blue box left-middle in Figure~\ref{fig:topic-distiller-dfd}) takes the sorted graph from Graph Builder and scores and ranks
the nodes according to the \emph{topic overlap score} ($\mathrm{tos}$) defined
in Equation~\ref{eq:final-topic-score}. The Ranking module consists of two main
functions: \texttt{get\_topic\_overlap\_score}, and \texttt{rank\_topics}. The
former is used to calculate $\mathrm{tos}$ for each node or topic and latter
for ranking the topics using the \emph{Final Topic Score} ($\mathrm{fts}$)
defined in Equation~\ref{eq:final-topic-score}. The tokenization needed for
computing $\mathrm{tos}$ is provided by the \emph{spaCy} library.
