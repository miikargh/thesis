The goal of the evaluation, detailed in Chapter~\ref{ch:evaluation},
was to check that the requirements, set in Section~\ref{sec:requirements}, were
fulfilled by the Topic Distiller. Additionally, the effect of machine translation
to the performance of the Topic Distiller was evaluated.

This chapter discusses the results of that
evaluation recalling the design and implementation details of
Chapter~\ref{ch:design}.

\section{General Discussion}
Tables~\ref{tab:hulth2003-fpr} and~\ref{tab:semeval2010-fpr}, along with Figures~\ref{img:hulth2003-prf-plots} and~\ref{img:semeval2010-prf-plots} reveal that the Topic Distiller is not the best candidate for extracting topics from scientific articles. The TopicRank algorithm dominates on performance in this domain with MultipartiteRank as close second. In terms of precision, recall and F-score, the Topic Distillers performs worst of all the candidates but achieves comparable results with R-precision with PartOf method.

Topic Distiller redeems itself by achieving best performance on the semeval2010
dataset when measured with R-precision using the Exact and PartOf strategies, as can be seen
from Table~\ref{tab:semeval2010-rpre}. However, the Topic Distiller looses to PositionRank when
measured with R-precision using the Includes strategy and to TopicRank when precision, recall and F-score are in question, as is evident from Table~\ref{tab:semeval2010-fpr}.

By far the best performance of Topic Distiller is achieved with the guardian and
tweet datasets, as shown in
Tables~\ref{tab:guardian-fpr},~\ref{tab:tweets-fpr},~\ref{tab:guardian-rpre},
and~\ref{tab:tweets-rpre}. Here, the Topic Distillers performance far surpasses
any of the comparison algorithms by all metrics, except for R-precision with
strategy Includes measured on the tweets dataset,
where TopicRank is the best performing method. The ability to find topics
from news articles and, especially, from social media postings is very important
in the modern world, because of the overwhelming quantity of this type of data
freely available on the internet.

As a summary, the evaluation test results of Section~\ref{sec:evaluation-results} show that
the Topic Distiller is able to identify both relevant and latent topics in
textual content. It works best on news articles and social media postings
compared to the state-of-the-art methods but it is unable to reach the preformance of those methods when evaluated on abstracts of scientific articles.

\section{Satisfying the Requirements}
This section takes apart the requirements listed in
Section~\ref{sec:requirements} and checks that the Topic Distiller fulfills them
one by one.

\subsection{Is the Topic Distiller Able to Identify Relevant and Latent Topics in Textual Content?}\label{sec:discussion-q1}

None of the compared state-of-the-art methods were able to find any of the
latent keyphrases from the datasets. Only Topic Distiller was able to do so. This stems from the fact that these methods only \emph{extract} keyphrases from documents. They may be able to combine the words of the document in novel ways but the vocabulary is strictly restricted to the document processed.

The poor performance of all algorithms on the semeval2010 dataset is most likely do to the fact that large portions of the documents in the datasets are not natural human language.

\subsection{Is the Topic Distiller Able Extract Topics From Short Texts Such as Social Media Postings?}\label{sec:discussion-q2}
The evaluation results on tweets dataset, seen in Tables~\ref{tab:tweets-fpr},
and~\ref{tab:tweets-rpre}, make it clear that the Topic Distiller is not only
able to identify topics in tweets, but that it exceeds the
performance of state-of-the-art methods by a significant degree. Though the
system was not evaluated on Facebook data, it is reasonable to assume that the
Topic Distiller would be able to process Facebook status posts, along with other
short social media postings, as well.

\subsection{Is the Topic Distiller Able to Process Enough Documents Per Minute?}
Figure~\ref{img:processing-time-plots} shows that all algorithms show significant increase in processing time when document length is increased and as the result, the number of documents processed per minute decreases. Conversely, the number of extracted keyphrases does not seem to affect the processing time immensly in any of the algorithms. Though not the swiftest algorithm available, Topic Distiller is able to process a reasonable amount of documents per minute.

\subsection{Does Machine Translation Have an Effect on the Output of the Topic Distiller?}
Tables~\ref{tab:org-trn-5kps},~\ref{tab:org-trn-10kps},
and~\ref{tab:org-trn-15kps} show that using machine translation significantly
decreases the performance of the Topic Distiller. This is evident from decrease
of all performance metrics when machine translated documents are compared to the
original ones. That being said, the Topic Distiller was still able to identify
some of the original keyphrases using the translated documents making it at
least a reasonable fall-back if no other methods are found for non-English
texts.


\section{Future Work}
The current version of the Topic Distiller has three tunable parameters:
\emph{the number of hops} used to traverse the DBpedia graph (see Section:~\ref{sec:ontology-design}), and the
coefficients \emph{$\alpha$} and \emph{$\beta$} (see Section:~\ref{sec:ranker-design}), former of which is used to
weight the importance of degree centrality in ranking and latter for the
importance of topic overlap score. However, the effect of tuning these
parameters was not evaluated. This calls for further analysis in later work.

The current version of the Topic Distiller is only able to process data in English language. The authors of DBpedia provide the ability to compile DBpedia with all languages available in Wikipedia. The performance of the Topic Extracor using different language datasets should be researched. The integration of ontologies apart from DBpedia, such as Wikidata and Yago, should also be investigated.

An interesting research problem would be to combine the power of neural networks
with the vast knowledge of ontologies for AKE.~The combination of neural
networks to knowledge graphs have already been researched and systems such as
Graves et al.'s \emph{Differentiable Neural Computer}~\cite{graves2016hybrid}
and Pham et al.'s \emph{Graph Memory Networks}~\cite{pham2018graph} show very
promising results. These neural networks are able to \emph{reason} by learning
how to traverse konwledge graphs. This reasoning ability would most likely be
beneficial for AKE as well. Using deep learning to connect deep neural networks
to knowledge bases should not be confused with the deep learning metrics
discussed in Section~\ref{sec:deep-learning}, which focus only on extracting or
generating keyphrases from text instead of using knowledge bases as basis for
logical reasoning. Alas, the lack of large datasets also prohibit these methods
to reach their potential.
Deep learning has been very successful in the area of
machine vision where huge datasets of labeled data, such as
ImageNet\footnote{http://www.image-net.org/}, are freely available. Future work
should be directed towards building such datasets for natural language
processing tasks, such as AKE, as well.