% This section contains different evaluations that measure if the implemented
% project is able to satisfy the requirements stated in
% Section~\ref{sec:requirements}:
The scope of this evaluation was to check whether the implemented described in
Chapter~\ref{ch:design} satisfies the requirements listed in
Section~\ref{sec:requirements}. The evaluation described in this chapter provides answers to
the questions listed further below.

The first and most important question: \emph{Is the Topic Distiller able to
identify relevant and latent topics in a textual document?} To answer this, the
Topic Distiller is evaluated using the conventional methods discussed
in Section~\ref{sec:evaluation-ake}. These methods include precision ($\mathrm{p}$),
recall ($\mathrm{r}$), F-score ($\mathrm{f}$), and R-precision ($\mathrm{p_R}$). The aforementioned metrics are used to compare the performance of Topic Distiller against a set of state-of-the-art AKE-algorithms listed in Section~\ref{sec:test-setup}.

The second question to arise is: \emph{Is the Topic Distiller able to extract
topics from social media postings?} This question is more problematic since no
available dataset for social media postings with human annotated keyphrases were
found. In order to evaluate the performance of the Topic Distiller on social
media postings a dataset of 70 human annotated tweets was created (see Section:~\ref{sec:datasets}).

The third question is: \emph{Do the topics discovered by the Topic Distiller
represent real world phenomena}? Fortunately, the usage of EL guarantees this
and no evaluation of this requirement is needed.

Performance testing was conducted to answer the question number 4: \emph{Is the Topic
Distiller able to process documents fast enough?} The only performance metric of interest was the throughput of the system, or how many documents the system is able to process in a time period.

The fifth question would be: \emph{Is the Topic Distiller easy to maintain?}
This question is hard to answer by evaluation. Instead, a number of design
principles, described in Section~\ref{sec:design-principles}, were adopted to make
the system as maintainable as possible.

The question number six is: \emph{Does the Topic Distiller provide a simple
  interface for communicating with other systems?} No evaluation was conducted
on the simplicity of the API provided by the Topic Distiller, but it is easy to
see that the API is simple enough by looking at its design in Section~\ref{sec:api-design}.

Additionally, a seventh question is added to the evaluation: \emph{Does machine
translation affect the performance of the Topic Distiller?} This is an important
question since the Topic Distiller, in its current form, is only able to process
documents written in English. This clearly poses a problem to processing
documents written in other languages. One possible way to handle non-English
documents is to first translate them to English and them feed them to the Topic
Distiller. While machine translation keeps improving from year to year it still
is not perfect, making the machine translated document somewhat different from a human translated one.

To evaluate the effects of machine translation on the performance of the Topic Distiller a special dataset, described in Section~\ref{sec:datasets}, is collected. This dataset includes articles written both in Portugese and English. The Portuguese articles are then translated to English using Google Translate service\footnote{\url{https://translate.google.com/}}. Then the performance of the Topic Distiller is measured using original (English) and the translated versions of the articles and the results compared. 


To summarize, here are the questions to be addressed in this evaluation:
\begin{enumerate}
\item Is the Topic Distiller able to identify relevant and latent topics in a textual document?
\item Is the Topic Distiller able to extract topics from short texts such as
  social media postings?
\item Is the Topic Distiller able to process enough documents fast enough?
\item Does machine translation have an effect to the output of the Topic Distiller?
\end{enumerate}


\section{Test Setup}\label{sec:test-setup}
This section describes the evaluation setup including the methods and data used.

\subsection{Datasets}\label{sec:datasets}
Five datasets were used to evaluate the Topic Distiller. Two of the datasets
(hulth2003 and semeval2010) are from the literature. The other three datasets
(guardian, tweets, wikinews)
are composed for the purposes of this thesis. More information on the
composition of these three datasets can be found in the appendix (see Section~\ref{ch:appendix}).  A summary of datasets
can be found in table~\ref{tab:dataset-descs} and descriptions of said datasets
are presented further in this section.

\begin{table}[ht]
    \caption{Dataset descriptions\label{tab:dataset-descs}}
    \begin{subtable}[b]{1.0\textwidth}
        \centering
        \caption{\label{tab:dataset-desc-1}}
        \begin{tabular}{llr}
                    Name &                   Type &  \# of documents \\
            \hline
               hulth2003 &              Abstracts &             500 \\
             semeval2010 &    Scientific articles &             144 \\
                guardian &          News articles &             190 \\
                  tweets &  Social media postings &              60 \\
                wikinews &          News articles &             135 \\
        \end{tabular}
    \end{subtable}
    \hfill
    \begin{subtable}[b]{1.0\textwidth}
        \centering
        \caption{\label{tab:dataset-desc-2}}
        \begin{tabular}{lrrrr}
                    Name &   Avg KPD &  Max KPD &  Min KPD &  \% in Wikipedia \\
            \hline
               hulth2003 &  9.15 &       28 &        1 &       13.3 \\
             semeval2010 &  3.88 &       13 &        2 &       26.7 \\
                guardian &  5.75 &       19 &        2 &       48.4 \\
                  tweets &  4.33 &        9 &        1 &       46.5 \\
                wikinews &  9.13 &       26 &        3 &       38.9 \\
        \end{tabular}
    \end{subtable}
    \hfill
    \begin{subtable}[b]{1.0\textwidth}
        \centering
        \caption{\label{tab:dataset-desc-3}}
        \begin{tabular}{lrrrr}
                    Name &    Avg length &  Max length &  Min length &  \% of latent \\
            \hline
               hulth2003 &    805 &        1843 &         105 &    23.1 \\
             semeval2010 &  48100 &       86483 &       16707 &    25.9 \\
                guardian &   4070 &       39334 &         964 &    52.8 \\
                  tweets &    137 &         288 &          43 &    44.6 \\
                wikinews &   4740 &       18163 &        2110 &    42.4 \\
        \end{tabular}
      \end{subtable}
\end{table}

The \emph{Type} column of Table~\ref{tab:dataset-desc-1} describes what sort of
document the dataset consists of and the \emph{\# of documents} indicates how
many documents each datasets has. The \emph{Avg KPD} column tells the average
number of keyphrases per document and the \emph{Max KPD} and \emph{Min KPD}
columns of \ref{tab:dataset-desc-2} indicate what are the maximum and minimum
number of keyphrases per document respectively. Similarly the \emph{Avg length},
\emph{Max length}, and \emph{Min length} columns of
Table~\ref{tab:dataset-desc-3} express the average, maximum, and minimum length
of documents, measured by characters, found in a dataset. The \emph{\% in
  Wikipedia} column of Table~\ref{tab:dataset-desc-2} depicts the number of
keyphrases that are also Wikipedia article titles\footnote{The set of Wikipedia
  article titles was downloaded from \\
  https://dumps.wikimedia.org/enwiki/latest/enwiki-latest-all-titles.gz on 22nd
  of January 2019}. This information is relevant since the Topic Distiller is
only able to find topics that are also Wikipedia article titles. The \emph{\% of
  latent} column of Table~\ref{tab:dataset-desc-3} shows which percentage of keyphrases in the dataset are latent in their respective documents.

\begin{enumerate}
\item\textbf{Hulth2003} \\
The Hulth2003 (hulth2003)~\cite{hulth2003improved} is a dataset of 2000 scientific article abstracts in English. The abstracts were collected from journal papers during the years 1998-2002 and the disciplines include \emph{Computers and Control}, and \emph{Information Technology}. The 2000 articles are randomly split into three sets: \emph{training}, \emph{testing}, and \emph{validation}, each consisting of 1000, 500, and 500 abstracts respectively. We used the 500 abstracts from validation dataset. The maximum number of keyphrases per document in the validation set is 28 and minimum 1. The longest document is 1843 characters long, while the shortest is only 105 characters. Latent keyphrases are at 23.1\% of all the keyphrases in this dataset.

\item\textbf{SemEval-2010 Task 5} \\
The SemEval-2010 Task 5 (semeval2010)~\cite{kimsemeval} dataset consists of conference and workshop papers totaling 284 documents. The documents fall into the following disciplines: \emph{Distributed Systems}, \emph{Information Search and Retrieval}, \emph{Distributed Artificial Intelligence – Multiagent Systems}, and \emph{Social and Behavioral Sciences – Economics}.

In contrast to hulth2003 dataset, this datasets contains the whole articles, including the abstract. The data is split into \emph{trial}, \emph{train}, and \emph{test} sets with 40, 144, and 100 articles. The trial data is a subset of the training data which decreases the amount of unique documents to 244 documents. In this work, the train dataset is used. Most keyphrases per document in the train set is 13 and the least keyphrases per document is 2. 86483 is the length of the longest document, measured in characters, the shortest being 16707. Latent keyphrases make 25.9\% of the total keyphrases.

\item\textbf{Guardian} \\
The Guardian (guardian) dataset is the first of the three datasets created specifically for this work. The dataset consists of 200 news articles collected from The Guardian\footnote{\url{https://theguardian.com}} web news site during August of 2018. All of the articles include annotations for topics present in the article as can be seen from Figure \ref{fig:guardian-topics}. 19 is the maximum number of keyphrases per document and 2 is the minimum. The longest document is 39334 characters long and the shortest lies only at 964 characters. The percentage of latent keyphrases of total keyhprases is 52.8.

In addition to consisting of news articles instead of scientific articles or abstracts, the keyphrases in the guardian dataset are often absent from the documents themselves. This fact makes the guardian dataset a good candidate for evaluating the performance of AKE algorithms on latent keyphrases.

Unfortunately, due to copyright issues, this dataset cannot be distributed.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/guardian_topics.png}
\caption{\label{fig:guardian-topics}
The Guardian article topics source:
\protect\url{https://www.theguardian.com/world/2018/oct/01/japan-us-military-base-critic-voted-in-as-okinawa-governor
.}}
\end{figure}

\item\textbf{Tweets} \\
The Tweets (tweets) dataset is also created for this evaluation. It consists of 60 randomly selected tweets collected on 20th of September of 2018. The tweets were annotated by the author of this work. The annotations include keyphrases that are present in the tweets themselves and some keyphrases that cannot be found in the documents. Most keyphrases per document resides at 9 and least at 1. The longest tweet is 288 characters in length while the shortest is 43 characters long. 44.6\% of all keyphrases in this dataset are latent.

The distribution of this dataset is also prohibited by the copyright issued by Twitter.

\item\textbf{Wikinews} \\
Like the guardian and tweets datasets, the wikinews dataset is also composed for the purpose of evaluating the Topic Distiller. It consists of 135 articles collected from wikinews\footnote{\url{https://en.wikinews.org/}}. What sets this particular dataset apart from the others, is the fact that this dataset consists of sets of news articles both in Portuguese and English. The purpose of this dataset is to enable evaluation of the effect of machine translation on different metrics of evaluation. The test setup, which employs this dataset, is detailed in Section~\ref{sec:effect-of-translation}. Maximum number of human annotated keyphrases in a document lies at 26 and minimum at 3, while lengths of the document reside between the minimum of 2110 characters and maximum of 18163 characters. The percentage of latent keyphrases of all keyphrases is 42.4.
\end{enumerate}

\subsection{Data Preparation}
The only preprocessing done to the documents and their keyphrases is lowercasing. No further preprocessing is done to emulate the messy reality where it is difficult to know what kind of textual data might be fed to the system.

\subsection{Latent Keyphrases}
Evaluating the ability of AKE methods to discover latent keyphrases, or keyphrases that are not to be found in the documents themselves, needs special consideration. For example, should a keyphrase be labeled as present if parts of it are present in the document? Or should the keyphrase be thought as latent if it is constructed from words all found within the document but the keyphrase itself is not in the document?

In this work, a keyphrase is classified as present if all of its component words are found within the document and latent if one ore more words are missing in the document. Non alphanumerics are removed from the keyphrases before evaluating if they are latent or not.

To evaluate the performance of AKE methods on discovering latent keyphrases a modified versions of the datasets, of section \ref{sec:datasets}, are created. In these modified datasets the present keyphrases are removed leaving only the latent ones.

\section{Metrics}\label{sec:evaluation-metrics}
This section details the metrics used to evaluate the Topic Distiller.

\subsection{Precision, Recall, F-score, and R-precision}\label{sec:prfr-metrics}
To evaluate the performance of Topic Distiller versus state-of-the-art models we
used the metrics $\mathrm{p}$, $\mathrm{r}$, $\mathrm{f}$, and $\mathrm{p_R}$, defined in Section~\ref{sec:evaluation-ake} by equations~\ref{eq:precision},~\ref{eq:recall},~\ref{eq:fscore},~\ref{eq:rprecision} respectively.

The metrics, $\mathrm{p}$, $\mathrm{r}$, and $\mathrm{f}$, are computed first with 5 extracted keyphrases and then with 10 and 15 extracted keyphrases.

The metric $\mathrm{p_R}$ is computed three times per dataset using the three different
strategies, \emph{Exact}, \emph{Includes}, and \emph{PartOf}, proposed
by~\cite{zesch2009approximate}. The Exact strategy compares keyphrases as they
are; only \emph{exact} matches are counted as correct ones. The Includes method
also considers extracted keyphrases that include a standard one as a substring
as correct. For example, an extracted keyphrase \emph{neural machine
  translation} would be considered as correct if the corresponding standard
keyphrase is \emph{machine translation}. Conversely, the PartOf method considers
an extracted keyphrase as correct if it is a part of an standard keyphrase. For
example, an extacted keyphrase \emph{artifical intelligence} is considered
correct if the standard is \emph{general artifical intelligence}. For each
strategy, lemmatization is used to reduce the words in the keyphrases into their
base forms. For precision, recall, and f-score no such lemmatization is used. To
make the lengths of the lists of extracted and standard keyphrases equal in
length 15 keyphrases are extracted using each method and the length of the
longer list of keyphrases is truncated to match the length of the shorter list.
For example, if the length of standard keyphrases is 12 then three keyphrases from the end of the extracted keyphrases are cut off. 

In addition to the aforementioned metrics, a modified version of each method, where the ranking is not taken into account, is introduced. Instead correct keyphrases are computed as the intersection of sets of standard and extracted keyphrases. These modified metrics are used to evaluate how much of the latent keyphrases the systems are able to identify.

\subsection{Processing Time}
Two tests are composed for evaluating the processing time of the algorithms. The
first test evaluates the effect of document length on the processing time. The
test is devised so that a document is truncated to be only 1000 characters of
length. Then, 1000 characters are added at each step and the processing time is
recorded. At each step the measurement is performed ten times to average out
possible fluctuations of time caused by the underlying operating system. The document used in this test is the longest of the documents in the guardian dataset.

The second test evaluates how the number of keyphrases extracted affects the
processing time. This is done by incrementing the number of keyphrases from one
to 20. Again, at each step the measurement is repeated ten times and the final
value is the average of those ten times.

All processing time tests were conducted on an Apple MacBook Pro (15-inch, 2018)
laptop with 2,6 GHz Intel Core i7 processor and 16 GB 2400 MHz DDR4 memory. The
operating system used was macOS Mojave version 10.14.3.

\section{State-of-the-art AKE methods}\label{sec:ake-algorithms}
To compare the performance of state-of-the-art AKE methods to the Topic Distiller, we employ the pke – python keyphrase extraction~\cite{boudin2016pke} toolkit which contains implementations for multiple state-of-the-art AKE methods. We use the methods described in Table~\ref{tab:ake-algorithms}.

\begin{table}[htbp]
\caption{\label{tab:ake-algorithms}
AKE algorithms used in evaluation}
\centering
\begin{tabular}{lll}
Name & Author(s) & Method\\
\hline
Topical PageRank & Sterckx et al.~\cite{sterckx2015topical} & Topical information with PageRank\\
PositionRank & Florescu et al.~\cite{florescu2017positionrank} & Word position with PageRank\\
TopicRank & Bougouin et al.~\cite{bougouin2013topicrank} & Topic clusters with PageRank\\
MultipartiteRank & Boudin~\cite{boudin2018unsupervised} & Multipartite Graphs with PageRank\\
\end{tabular}
\end{table}

These methods include only unsupervised graph-based AKE methods. This makes sense since the method proposed in this work is also an unsupervised graph-based method.

The author of the pke toolkit, Boudin, evaluates the performance of the
algorithms implemented in the toolkit with the semeval2010 test dataset using
F-score and 10 top keyphrases without taking ranking of the keyphrases into
account. The version of the standard keyphrases Boudin uses in the evaluation
are the combined author- and reader-assigned keyphrases. Stemming is also used
in both standard and extracted keyphrases. The results of this evaluation can be
seen in Table~\ref{tab:pke-author-eval}.

\begin{table}[htbp]
\caption{\label{tab:pke-author-eval}
Evaluation of pke algorithms by Boudin fetched from: \protect\url{https://github.com/boudinfl/pke} in November 2018}
\centering
\begin{tabular}{lr}
Name & F-score\\
\hline
Topical PageRank & 0.031\\
PositionRank & 0.067\\
TopicRank & 0.119\\
MultipartiteRank & 0.142\\
\end{tabular}
\end{table}

\begin{table}[htbp]
\caption{\label{tab:pke-reproduced-eval}
Reproduced evaluation of pke algorithms}
\centering
\begin{tabular}{lr}
Name & F-score\\
\hline
Topical PageRank & 0.029\\
PositionRank & 0.065\\
TopicRank & 0.118\\
MultipartiteRank & 0.138\\
\end{tabular}
\end{table}

A reproduction of Boudin's evaluation
was conducted to make sure that the results Boudin reports are valid and that
the pke toolkit is safe to use in the evaluation of the Topic Distiller. Results
of this reproduction are found in Table~\ref{tab:pke-reproduced-eval}. By
comparing the F-scores, reported by Boudin (see Table~\ref{tab:pke-author-eval}), to the reproduced F-scores
(see Table~\ref{tab:pke-reproduced-eval}), it can be noted that the use of pke is
safe, since the scores differ only marginally. 

\section{Effect of Machine Translation}\label{sec:effect-of-translation}
The effect of machine translation on the performance of the Topic Distiller was
also evaluated. This was done by measuring precision, recall, F-score, and
R-score with two different sets of documents. The first set are English news
articles and the second set are the same articles written in Portuguese and then
translated to English using the Google Translate\footnote{https://translate.google.com/} machine translation service. Then the
aforementioned metrics are computed using both the original English version and
the translated English version in order to see the impact of machine translation
on this system.

This evaluation is carried out because, as it is described in this thesis, the
Topic Distiller is only able to extract keyphrases from documents written in English. Machine
translation is one possible solution to extract keyphrases from documents
written in other languages.

\section{Results}\label{sec:evaluation-results}
This section lists the results of the evaluation described in Section~\ref{sec:test-setup}.

\subsection{Precision, Recall, and F-score}\label{sec:prf-results}
This section lists the results of $\mathrm{p}$, $\mathrm{r}$, and $\mathrm{f}$ measurements. The results are grouped so that each dataset has its own page where the evaluation results are presented along with a short explanation. The highest score in each column is in bold and the method that has the highest mean score is in bold also. Note that the results of measurements with 10 keyphrases are omitted from the tables for brevity but are still visible on the plot figures.

\newpage
\subsubsection{hulth2003}
The results depicted in Table~\ref{tab:hulth2003-fpr} and
Figure~\ref{img:hulth2003-prf-plots} show that the Topic Distiller is not
optimal for extracting keyphrases from abstracts of scientific articles. Far
better results can be achieved using conventional methods from which TopicRank
has the top performance. The poor performance of Topic Distiller could be
attributed to two factors: 1.~Many of the keyphrases in the hulth2003 dataset (86.7\%, see Table~\ref{tab:dataset-desc-2}) can not be found in the wikipedia as article titles. 2.~The keyphrases in the hulth2003 dataset are very specific and the Topic Distiller is biased towards more general concepts.
\input{tables/hulth2003_validation_rest_5_15.tex}
\input{figures/hulth2003_prf_plots.tex}

\newpage
\subsubsection{semeval2010}
The evaluation results on the semeval2010 dataset seen in Table~\ref{tab:semeval2010-fpr} and Figure~\ref{img:semeval2010-prf-plots} show that none of the algorithms compared perform very well on this dataset. Only TopicRank and MultipartiteRank are able to extract any matches and even their performance is an order of magnitude worse than with the hulth2003 dataset. This is due to the fact that, unlike the hulth2003 dataset, semeval2010 dataset consists of whole scientific articles with non-human language, like mathematical notation, included, making the extraction process much harder.
\input{tables/semeval2010_train_evaluation_rest_5_15.tex}
\input{figures/semeval2010_prf_plots.tex}

\newpage
\subsubsection{guardian}
As it can be noted from looking at Table~\ref{tab:guardian-fpr} and Figure~\ref{img:guardian-prf-plots}, Topic Distiller is the only algorithm that scores above zero in precision, recall and F-score when evaluated on the guardian dataset. The reason for this stems from the facts that the guardian dataset contains the highest percentage of latent keyphrases (52.8\%, see Table~\ref{tab:dataset-desc-3}) and that almost half (48.4\%, see Table~\ref{tab:dataset-desc-2}) of the keyphrases are also Wikipedia article titles.

The keyhprases in the guardian dataset are also more general, opposed to being specific, than in the hulth2003 and semeval2010 datasets making Topic Distiller more suitable for finding these keyphrases.
\input{tables/guardian_evaluation_rest_5_15.tex}
\input{figures/guardian_prf_plots.tex}

\newpage
\subsubsection{tweets}
Looking at Table~\ref{tab:tweets-fpr} and Figure~\ref{img:tweets-prf-plots} it
is clear that Topic Distiller dominates in performance when tweets are in
question. This can be attributed to the fact that tweets are very short texts.
Also the tweets dataset contains high percentage of latent keyphrases (44.6\%,
see Table~\ref{tab:dataset-desc-3}) and a large portion of the keyphrases are
also Wikipedia article titles (46.5\%, see Table~\ref{tab:dataset-desc-2}).
\input{tables/tweets_evaluation_rest_5_15.tex}
\input{figures/tweets_prf_plots.tex}
\clearpage

\subsection{R-precision}\label{sec:rpre-results}
This section contains the results of $\mathrm{p_R}$ measurements described in
Section:\ref{sec:prfr-metrics}. The
Tables~\ref{tab:hulth2003-rpre},~\ref{tab:semeval2010-rpre},~\ref{tab:guardian-rpre},
and~\ref{tab:tweets-rpre} presenting the $\mathrm{p_R}$ measurements clearly show that
$\mathrm{p_R}$ is more forgiving metric than $\mathrm{p}$, $\mathrm{r}$,
$\mathrm{f}$ (see
Tables~\ref{tab:hulth2003-fpr},~\ref{tab:semeval2010-fpr},~\ref{tab:guardian-fpr},~\ref{tab:tweets-fpr})
are. This was expected since $\mathrm{p_R}$ allows for more leeway when the automatically discovered
keyphrases are matched against the gold standard ones, as detailed in Section~\ref{sec:evaluation-metrics}.\\

\subsubsection{hulth2003}
As it was the case with $\mathrm{p}$, $\mathrm{r}$, and $\mathrm{f}$, the TopicRank (TR) performs the best on scientific abstracts when measured with R-precision as seen in Table~\ref{tab:hulth2003-rpre}. MultipartiteRank (MR) comes as close second scoring best when measured with \emph{PartOf} strategy. The Topic Distiller (TD) has the worst performance of the methods but achieves scores comparable to the top methods when measured with the \emph{PartOf} strategy.
\input{tables/hulth2003_validation_rpre_5_15.tex}

\subsubsection{semeval2010}
R-precision measured on the semeval2010 dataset, seen in Table~\ref{tab:semeval2010-rpre}, shows interesting results as the top performer is not TopicRank (TR) as usual. Here, the Topic Distiller (TD) scores highest points when measured with the \emph{Exact} strategy, MultipartiteRank (MR) achieves best performance with the \emph{PartOf} strategy, and PositionRank (PR) with the \emph{Includes} strategy.
\input{tables/semeval2010_train_evaluation_rpre_5_15.tex}

\subsubsection{guardian}
By looking at the Table~\ref{tab:guardian-rpre}, it is evident that the Topic Distiller achieves by far the best scores in the guardian dataset. Worth mentioning is the fact that all the other methods scored zero points on this dataset when measured with the conventional precision, recall, and F-score. This combined with the fact that relative change in performance measured with the three different strategies – especially the difference between the \emph{Exact} strategy and the other two strategies – is not very large indicates that the list of keyphrases extracted and the list of standard keyphrases in the dataset are of different lengths. 
\input{tables/guardian_evaluation_rpre_5_15.tex}

\subsubsection{tweets}
The Table~\ref{tab:tweets-rpre} shows that, again, the Topic Distiller dominates when tweets are in question, but the gap between performance of Topic Distiller to other methods is not as large as in the guardian dataset.
\input{tables/tweets_evaluation_rpre_5_15.tex}


\subsection{Latent Keyphrases}
The Table~\ref{tab:latent} clearly shows that the Topic Distiller (TR) is the only method present in the comparison able to find latent keyphrases. The semeval2010 dataset is evidently the hardest dataset to extract keyhprases from. None of the methods was able to find any of the latent keyphrases from it. The Topic Distiller performs best on the tweets dataset by a large margin. Worth noting is that the keyphrases of the tweets dataset were annotated by the author of this thesis, so there might be a bias present skewing the results.

\clearpage

\begin{table}
\centering

\input{tables/hulth2003_latent_5_15_subtable.tex}
\input{tables/semeval2010_latent_5_15_subtable.tex}
\input{tables/guardian_latent_5_15_subtable.tex}
\input{tables/tweets_latent_5_15_subtable.tex}

\caption{\label{tab:latent}Precision ($\mathrm{p}$), recall ($\mathrm{r}$), and F-score ($\mathrm{f}$) evaluated on latent keyphrases with 5 and 15 keyphrases}
\end{table}

\clearpage

\subsection{Processing Time Performance}
This section covers the evaluation results on the processing time performance.

\input{figures/timing_plots.tex}

As it can be seen from Figure~\ref{img:processing-time-plots} and Tables~\ref{tab:kps-len-timing},~\ref{tab:kps-len-ppm},~\ref{tab:doc-len-timing}, and~\ref{tab:doc-len-ppm}, TopicRank (TR) and PositionRank (PR) are the fastest of the methods in this comparison. Slowest are the Topic Distiller (TD) and TopicalPagerank (TPR). That said, all algorithms are able to extract keyphrases in a reasonably short amount of time, that is, they work at a throughput that can not be reasonably expected from any individual human being.

It is also evident that the number of extracted keyphrases does not have an
impact on the processing time in any of the methods. This is due to the fact
that the number of keyphrases extracted is set by taking the top $n$ keyphrases from a set
of keyphrase candidates. Selecting the top 5 candidates takes no longer than
choosing 15 or 20 of the top candidates.

Document length, on the other hand, is inversely proportional to the processing
time. This is expected, since more text means more data to analyze.
MultipartiteRank (MR) is most affected by the length of the documents while TD
does not take a big hit on the processing time when the length of the document
increases.

It should be noted that very fluctuating behavior of TR, MR, and PR seen in
Figure:~\ref{img:kps-len-ppm-plot} is due to the fact that these
algorithms are so fast that slight changes in processing speeds, caused by the
underlying operating system, result in
drastic difference in the amount of documents processed, even when the results
are an average of multiple measurements.

\input{tables/kps_len_timing.tex}
\input{tables/kps_len_ppm.tex}


\input{tables/doc_len_timing.tex}
\input{tables/doc_len_ppm.tex}

\clearpage

\subsection{The Effect of Translation}\label{sec:results-translation}
This section lists the results of the evaluation of the effect of translation on the different evaluation metrics.

The Tables~\ref{tab:org-trn-5kps},~\ref{tab:org-trn-10kps}, and~\ref{tab:org-trn-15kps} clearly show that the translation has a negative effect on the evaluation metrics. Precision takes the worst hit, while R-precision with the PartOf strategy is least affected by the translation.

% \input{tables/translated.tex}

\begin{table}[!htbp]
\caption{\label{tab:org-trn-5kps}
Original vs. translated with 5 keyphrases}
\centering
\begin{tabular}{lrrr}
Metric & Original & Translated & Relative change [\%]\\
\hline
Precision & 0.0222 & 0.0133 & -40.0\\
Recall & 0.0117 & 0.0081 & -30.5\\
F-score & 0.015 & 0.01 & -33.5\\
R-precision Exact & 0.013 & 0.0105 & -18.8\\
R-precision Includes & 0.0335 & 0.0282 & -15.9\\
R-precision PartOf & 0.0139 & 0.0121 & -12.6\\
\end{tabular}
\end{table}

\begin{table}[!htbp]
\caption{\label{tab:org-trn-10kps}
Original vs. translated with 10 keyphrases}
\centering
\begin{tabular}{lrrr}
Metric & Original & Translated & Relative change [\%]\\
\hline
Precision & 0.0126 & 0.0089 & -29.4\\
Recall & 0.013 & 0.0105 & -18.8\\
F-score & 0.0124 & 0.0095 & -23.4\\
R-precision Exact & 0.013 & 0.0105 & -18.8\\
R-precision Includes & 0.0335 & 0.0282 & -15.9\\
R-precision PartOf & 0.0139 & 0.0121 & -12.6\\
\end{tabular}
\end{table}

\begin{table}[!htbp]
\caption{\label{tab:org-trn-15kps}
Original vs. translated with 15 keyphrases}
\centering
\begin{tabular}{lrrr}
Metric & Original & Translated & Relative change [\%]\\
\hline
Precision & 0.0094 & 0.0066 & -29.6\\
Recall & 0.013 & 0.0105 & -18.8\\
F-score & 0.0106 & 0.008 & -24.6\\
R-precision Exact & 0.013 & 0.0105 & -18.8\\
R-precision Includes & 0.0335 & 0.0282 & -15.9\\
R-precision PartOf & 0.0139 & 0.0121 & -12.6\\
\end{tabular}
\end{table}