This section describes how the system was implemented to match the requirements
in Section~\ref{sec:requirements} and the design presented in Chapter~\ref{ch:design}.
The implementation described in this section is a stand-alone software designed
to be easily integrated to other systems. This is done by providing a HTTP API
suitable for software to software communications.


\section{Tools and libraries}
The programming language chosen for this project was Python (version 3.6). As
noted in Section~\ref{sec:design-principles}, Python is a highly modular language with a large collection of standard libraries. Scientific computing community has been using Python for a long time building a large quantity of libraries suitable for NLP.~Python is also very expressive and rather easy to learn, enabling easy collaboration with other programmers in the future of a project.

The libraries used in this work can be found in Table~\ref{tab:project-libraries}.

\begin{table}[htbp]
\caption{\label{tab:project-libraries}
Project libraries}
\centering
\begin{tabularx}{\linewidth}{llL}
Library & Usage & Description\\
\hline
spaCy \cite{spacy} & NLP & Provides a large set of functions for NLP\\
aiosparql \cite{aiosparql} & SPARQL & Provides an easy way to integrate SPARQL queries in python\\
NetworkX \cite{networkx} & Graphs & Provides functions for building and analysing network graphs\\
Flask \cite{flask} & API & Provides a set of utilities for building Web APIs\\
 &  & \\
\end{tabularx}
\end{table}

\subsection{spaCy}
spaCy~\cite{spacy} is an ``Industrial-Strength Natural Language Processing library''. The authors of the library clain it is the fastest NLP library in the world, in terms of computing time. spaCy provides a simple and unified API for its functions making it very easy to use. In this thesis, spaCy is used for text tokenization.

\subsection{aiosparql}
aiosparql \cite{aiosparql} is an asynchronous SPARQL wrapper for Python. It provides an easy way to integrate SPARQL queries to Python code. There are other SPARQL wrappers for Python but aiosparql is the only one providing asynchronous network calls making network call optimization effortless.

\subsection{NetworkX}
NetworkX \cite{networkx} is a ``Python package for the creation, manipulation, and study of the structure, dynamics, and functions of complex networks''. It is used in this project for building the graphs and analyzing the centrality of the nodes in the graphs.

\subsection{Flask}
Flask~\cite{flask} is a micro-framework for Python for building web APIs. This minimalist framework is very modular enabling the use of only those functions needed thus making APIs build with Flask light and simple.

\section{Internal modules}
The Python code in this work is split into modules, each providing a set of
functions that belong to a block seen in Figure~\ref{fig:system-block-diagram}. The design of the modules can be found in Section~\ref{sec:system-design}.

\subsection{Ontology}
This module connects the rest of the system to DBpedia. It provides the SPARQL
commands needed to traverse DBpedia along with the logic to do so. The main functions in the Ontology module are \texttt{extract\_dbpedia\_ents}, which queries DBpedia Spotlight for annotating mentions of DBpedia entities in the text, and \texttt{get\_edges}, which creates a list of linked DBPedia entities by recursively querying DBpedia given the entities annotated by \texttt{extract\_dbpedia\_ents}. The implementation of the Ontology module relies on the \emph{aiosparql} library.

\subsection{Graph builder}
The Graph Builder module comprises of functions that are used to build and analyse the network graph from the list of connected edges provided by the \texttt{get\_edges} function of the Ontology module. The main functions of the Graph Module are \texttt{create\_graph}, which takes in a list of edges and builds a graph from them, and \texttt{sort\_nodes} which sorts the nodes by their degree centrality. The implementation of the Graph Builder module utilises the \emph{NetworkX} module.

\subsection{Ranker}
The Ranker module takes the sorted graph from Graph Builder and scores and ranks
the nodes according to the \emph{topic overlap score} ($\mathrm{tos}$) defined
in Equation~\ref{eq:final-topic-score}. The Ranking module consists of two main
functions: \texttt{get\_topic\_overlap\_score}, and \texttt{rank\_topics}. The
former is used to calculate $\mathrm{tos}$ for each node or topic and latter
for ranking the topics using the \emph{Final Topic Score} ($\mathrm{fts}$)
defined in Equation~\ref{eq:final-topic-score}. The tokenization needed for
computing $\mathrm{tos}$ is provided by the \emph{spaCy} library.

\subsection{API}
This module simply exposes the \texttt{extract\_topics} function to outside
world with HTTP protocol using the \emph{Flask} micro-framework. The
\texttt{extract\_topics} function takes only two arguments: the text to be
analyzed and the number of topics to be returned. Detailed explanation of the design of the API can be found in Section~\ref{sec:api-design}.
