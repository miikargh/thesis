Since the invention of the world wide web, the amount of freely accessible data has exploded. News articles are published at an increasing rate along with blog posts, not to mention the gargantuan amount of data produced by social media.  To gain knowledge of this vast quantity of data manually is beyond the capabilities of single humans or even organizations, rendering the effort futile. To tackle the problem of making sense of this vast amount of information, automation must be exploited.

Most of the data are text-based and unstructured rendering it difficult to process them automatically. Computers are very good at processing well structured data but are lacking the ability to process natural language. Natural language processing is a discipline, closely entwined to artificial intelligence, that undertakes in the effort of augmenting computers with the ability to process natural human language. Such techniques as automatic topic modeling, summarization, and automatic keyphrase extraction are ways of distilling information from large quantities of data represented in natural human language. These techniques work as tools for humans to enhance their ability to make sense of this fast-paced world with its never-ending torrent of information.

A way to tackle the problem of making sense of the vast quantities of digital
information is to bring structure to the unstructured data. This can be achieved
by extracting semantic labels from the data and using these labels to index said
data. Indexing the data makes it more useful since it can then be more
efficiently queried. Three sub-disciplines of natural language processing have emerged to do just this: automatic keyphrase extraction (AKE), automatic topic labeling (ATL), and entity linking (EL). AKE focuses on extracting descriptive labels from single documents and ATL for collections of documents. Both AKE and ATL have researched a multitude of ways to extract essential information from textual data but the state-of-the-art suggests that employing graph theory is the way to go. This means representing text as network graphs and investigating the relationships of the nodes in those graphs. Representing text as graphs has another advantage: graphs can be connected to other graphs, such as knowledge bases. EL aims to do just this; connect documents to knowledge bases.

Semantic web~\cite{berners2001semantic} is a project that aims to bring structure to the world wide web. This project has given birth to large structure knowledge bases such as DBpedia~\cite{auer2007dbpedia} and Yago~\cite{suchanek2007yago}. These knowledge bases, also known as ontologies, are reservoirs of large quantities of connected data. Taking advantages of these ontologies has proven to be highly beneficial~\cite{nguyen2009ontology},~\cite{allahyari2009ontolda},~\cite{hulpus2013unsupervised}.

This thesis details the design and implementation of a system for distilling
information from textual documents: the \emph{Topic Distiller}. Namely, the
Topic Distiller is able to determine the topics discussed in a document and link
those topics to DBpedia knowledge base. The topics might be explicitly mentioned
in the document or they might be latent but \emph{implied}. For example, consider the following piece of text:

\begin{center}
\textit{Elon Musk is the CEO of Tesla.}
\end{center}

The explicit topics in the text are \emph{Elon Musk}, \emph{CEO}, and
\emph{Tesla}. The latent topics would be \emph{electric cars},
\emph{organizations}, and \emph{car manufacturing}.

To find the latent topics, the Topic Distiller combines EL with graph-based
methods inspired by AKE and ATL. First, the EL process annotates the entities in
a given text document. Second, more general topics are found by leveraging
DBpedia. Third, all the topics are combined into a network graph and
graph-based measures are used to determine which are the most relevant topics.

An application programming interface (API) to the Topic Distiller is provided so that
it can be deployed as a web service and used as a sub-component for larger systems.

